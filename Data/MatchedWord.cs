using System;
using System.Collections.Generic;

namespace WordUnscrambler.Data
{
    struct MatchedWord
    {
       public string ScrambledWord { get; set; }
       public string Word { get; set; }
    }
}
